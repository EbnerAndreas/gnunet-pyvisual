import subprocess
import os
import re
import time
import matplotlib.pyplot as plt
import networkx as nx
import numpy as np

backward = list()
forward = list()
bw_nodes = list()
fw_nodes = list()
start_nodes = list()
end_nodes = list()
bw_arr = np.array([['', ''], ['', '']], dtype="object")
fw_arr = np.array([['', ''], ['', '']], dtype="object")
subs_table = dict()


def add_entry(x, y, new_entry, previous_entry):
    # check if node already in 2d matrix
    print("New:", new_entry, " | Previous: ", previous_entry)
    if np.where(x == new_entry)[0].size > 0:
        print("Already in..")
        # if already in, break this for argument
        return x
    if np.where(y == new_entry)[0].size > 0:
        print("Already in other..")
        # if already in, break this for argument
        return x

    pos = np.where(x == previous_entry)
    # Expect node to be only once in x
    posX = pos[0][0]
    posY = pos[1][0]
    new_posY = posY + 1

    # Get current shape
    # TODO m, n = np.shape(A)[:2]
    shapeX = x.shape[0]
    shapeY = x.shape[1]
    # if FW add to left:
    # first: see if other exists on this x level, then switch to "free" or new x level
    if x[posX][new_posY] != '':
        print("Occupied, looking for new row..")

        # Look for free slot downwards
        for k in range(posX, shapeX):
            if x[k][new_posY] == '':
                x[k][new_posY] = new_entry
                break

        # add new bottom row with free slots if this node has been added to bottom row
        if k == shapeX - 1:
            tmp_arr = np.ndarray((1, shapeY), dtype="object")
            tmp_arr.fill('')
            x = np.r_[x, tmp_arr]
    else:
        # if we are at the last row add an additional to have space for later
        if new_posY + 1 == shapeY:
            # create new tmp array size of [1][Ymax]
            tmp_arr = np.ndarray((shapeX, 1), dtype="object")
            tmp_arr.fill('')
            x = np.c_[x, tmp_arr]

        x[posX][new_posY] = new_entry
    return x


def combine_arr(arr1, arr2):
    print("Combining...")
    tmp_arr1 = arr1
    tmp_arr2 = arr2
    m1, n1 = np.shape(arr1)[:2]
    m2, n2 = np.shape(arr2)[:2]

    # Adjust x-axis
    if m1 > m2:
        tmp_arr = np.ndarray((m1-m2, n2), dtype="object")
        tmp_arr.fill('')
        tmp_arr2 = np.r_[tmp_arr2, tmp_arr]
        m2 = m1
    elif m2 > m1:
        tmp_arr = np.ndarray((m2-m1, n1), dtype="object")
        tmp_arr.fill('')
        tmp_arr1 = np.r_[tmp_arr1, tmp_arr]
        m1 = m2

    # Adjst y-axis
    if n1 > n2:
        tmp_arr = np.ndarray((m2, n1-n2), dtype="object")
        tmp_arr.fill('')
        tmp_arr2 = np.c_[tmp_arr2, tmp_arr]
    elif n2 > n1:
        tmp_arr = np.ndarray((m1, n2-n1), dtype="object")
        tmp_arr.fill('')
        tmp_arr1 = np.c_[tmp_arr1, tmp_arr]

    # Mirror fw
    tmp_arr2 = np.fliplr(tmp_arr2)
    # Concatenate bw and fw
    tmp_conc_arr = np.concatenate((tmp_arr1, tmp_arr2), axis=1)
    # Remove all rows that only contain ''
    tmp_conc_arr = tmp_conc_arr[~np.all(tmp_conc_arr == '', axis=1)]
    # Remove all columns that only contain ''
    m, n = np.shape(tmp_conc_arr)[:2]
    del_arr = []
    for i in range(0, n):
        for j in range(0, m):
            if tmp_conc_arr[j][i] != '':
                # if contains something else than ''
                break
            if j == m-1:
                # if reach last entry and it's '', whole row is '', therefore delete it
                del_arr.append(i)

    tmp_conc_arr = np.delete(tmp_conc_arr, del_arr, axis=1)

    print("End combining...")
    return tmp_conc_arr


def add_del_node(graph, start_iss, start_role, end_subj):
    global fw_arr
    startNode = create_node_string(start_iss, start_role)
    endNode = create_node_string(end_subj, None)
    if np.where(fw_arr == endNode)[0].size == 0:
        fw_arr[0][0] = endNode
        end_nodes.append(endNode)
    end_nodes.append(startNode)
    add_node(graph, start_iss, start_role, end_subj, None, False)


def add_start_node(graph, start_iss, start_role):
    global bw_arr
    startNode = create_node_string(start_iss, start_role)
    bw_arr[0][0] = startNode
    start_nodes.append(startNode)
    add_node(graph, start_iss, start_role, None, None, True)

def create_node_string(node, role):
    # Either uses the first 3 symbols of the pub key
    # or the name if "gnunet-identity -d" is called at the start of the test
    if role is None:
        if node in subs_table:
            return subs_table[node].upper()
        else:
            return node[0:3]
    else:
        if node in subs_table:
            return subs_table[node].upper() + '.' + role
        else:
            return node[0:3] + '.' + role

def add_node(graph, start_node, start_role, end_node, end_role, bw):
    global fw_arr, bw_arr
    print("Start add...")

    # Create node names
    startNode = create_node_string(start_node, start_role)
    if end_node is None:
        # For starting node: only add node
        graph.add_node(startNode)
    else:
        # All other nodes: add node and edges (with graph.add_edge)
        # For delegates who don't have end_role
        endNode = create_node_string(end_node, end_role)

        # Add edge
        graph.add_edge(startNode, endNode)

        # Create edge list, used for drawing
        if bw:
            backward.append((startNode, endNode))
            bw_arr = add_entry(bw_arr, fw_arr, endNode, startNode)
            if endNode not in fw_nodes \
                    and endNode not in bw_nodes \
                    and endNode not in start_nodes \
                    and endNode not in end_nodes:
                bw_nodes.append(endNode)
        else:
            forward.append((startNode, endNode))
            fw_arr = add_entry(fw_arr, bw_arr, startNode, endNode)
            if startNode not in bw_nodes \
                    and startNode not in fw_nodes \
                    and startNode not in end_nodes \
                    and startNode not in start_nodes:
                fw_nodes.append(startNode)

    # combine existing arrays, preparation for plotting
    c_arr = combine_arr(bw_arr, fw_arr)

    # plot current graph
    plot_graph(graph, c_arr)


def plot_graph(graph, c_arr):
    print("Start plot...")

    # Clear plot for redrawing
    plt.clf()

    # Calculate position
    pos = dict()

    m, n = np.shape(c_arr)[:2]
    x_dist = 1/m
    y_dist = 1/n
    for i in range(0, m):
        for j in range(0, n):
            pos[c_arr[i][j]] = [j+1*y_dist, i+1*x_dist]

    # nodes
    nodes = nx.draw_networkx_nodes(graph, pos, node_size=1200, nodelist=end_nodes, node_color='blue', linewidths=3)
    if nodes:
        nodes.set_edgecolor('black')
    nodes = nx.draw_networkx_nodes(graph, pos, node_size=1200, nodelist=start_nodes, node_color='red', linewidths=3)
    if nodes:
        nodes.set_edgecolor('black')
    nx.draw_networkx_nodes(graph, pos, node_size=1200, nodelist=fw_nodes, node_color='blue')
    nx.draw_networkx_nodes(graph, pos, node_size=1200, nodelist=bw_nodes, node_color='red')

    # edges
    nx.draw_networkx_edges(graph, pos, edgelist=backward, width=6, alpha=0.5, edge_color='red')
    nx.draw_networkx_edges(graph, pos, edgelist=forward, width=6, alpha=0.5, edge_color='blue')

    # labels
    nx.draw_networkx_labels(graph, pos, font_size=10, font_family='sans-serif')

    # Show plot
    mng = plt.get_current_fig_manager()
    mng.resize(*mng.window.maxsize())
    plt.axis('off')
    plt.show(block=False)
    plt.pause(0.001)


def parse_sh_prints(line):
    line = line.decode("utf-8").replace('\n', '')

    # Parse start/end
    # FW end delegates
    matches = re.match("([\\d\\w]+)\\.([\\w]+) -> ([\\d\\w]+) \\| [=/+\\d\\w]+ \\| [\\d]+,?(.*)", line)
    if matches:
        while True:
            matches = re.match("([\\d\\w]+)\\.([\\w]+) -> ([\\d\\w]+) \\| [=/+\\d\\w]+ \\| [\\d]+,?(.*)", line)
            if matches:
                add_del_node(graph, matches.groups()[0], matches.groups()[1], matches.groups()[2])
            if not matches.groups()[3]:
                break
            line = matches.groups()[3]
    # Start of verify
    matches = re.match(".*--issuer=([\\d\\w]+).*--attribute=([\\w]+).*", line)
    if matches:
        add_start_node(graph, matches.groups()[0], matches.groups()[1])

    # Parse Intermediate
    # 1) has endrole
    matches = re.match("^(Backward|Forward).*Intermediate result: ([\\d\\w]+)\\.([\\w]+) <- ([\\d\\w]+)\\.([\\w]+)$",
                       line)
    if matches:
        add_node(graph,
                 matches.groups()[1],
                 matches.groups()[2],
                 matches.groups()[3],
                 matches.groups()[4],
                 True if "Backward" == matches.groups()[0] else False)
    # 2) has no endrole
    matches = re.match("^(Backward|Forward).*Intermediate result: ([\\d\\w]+)\\.([\\w]+) <- ([\\d\\w]+)$",
                       line)
    if matches:
        add_node(graph,
                 matches.groups()[1],
                 matches.groups()[2],
                 matches.groups()[3],
                 None,
                 True if "Backward" == matches.groups()[0] else False)
    # Parse Delegates
    matches = re.match("^([\\d\\w]+)\\.([\\w]+) <- ([\\d\\w]+)$", line)
    if matches:
        add_node(graph,
                 matches.groups()[0],
                 matches.groups()[1],
                 matches.groups()[2],
                 None,
                 False)
    # Parse Delegation Chain (if something is missing from Immediate)
    # TODO issue: no bw or fw displayed (?)
    '''matches = re.match("^\\([\\d]\\)\\s([\\d\\w]+)\\.([\\w]+) <- ([\\d\\w]+)\\.([\\w]+)$", line)
    if matches:
        print(matches.groups())'''

    # Parse substitution table
    "^([\\w]+) - ([\\d\\w]+)$"
    matches = re.match("^([\\w]+) - ([\\d\\w]+)$", line)
    if matches:
        subs_table[matches.groups()[1]] = matches.groups()[0]

if __name__ == '__main__':
    path = '/home/andi/gnunet/gnunet/src/credential/'
    cmd = 'GNUNET_FORCE_LOG=".*credential.*;;;;DEBUG/;;;;ERROR" ./test_credential_bi_and3.sh'
    #cmd = 'GNUNET_FORCE_LOG=".*credential.*;;;;DEBUG/;;;;ERROR" ./test_credential_bi_bw.sh'
    graph = nx.Graph()

    # Path management
    savedPath = os.getcwd()
    os.chdir(path)

    # Create process to handle bash file
    p = subprocess.Popen(cmd, shell=True, executable="/bin/bash",
                         stdout=subprocess.PIPE,
                         stderr=subprocess.STDOUT)

    # Poll stdout
    while p.poll() is None:
        # This blocks until it receives a newline
        lin = p.stdout.readline()
        #print(lin)
        parse_sh_prints(lin)
    # Cleanup possible rest
    lines = p.stdout.readlines()
    for lin in lines:
        #print(lin)
        parse_sh_prints(lin)

    # Set path back to pycharm project directory
    os.chdir(savedPath)
    print("Done: Showing result plot...")
    plt.show()
